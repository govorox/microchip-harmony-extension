<#macro DRV_GFX_ILI9341_DRIVER_DECLARATION IDX>
GFX_Result ILI9341_InfoGet(GFX_DriverInfo* info);
GFX_Result ILI9341_ContextInitialize(GFX_Context* context);
</#macro>

<#macro DRV_GFX_ILI9341_DRIVER_DEFINITION
	ID
	IDX>
	GFX_DriverInterfaces[${ID}].infoGet = &ILI9341_InfoGet;
    GFX_DriverInterfaces[${ID}].contextInitialize = &ILI9341_ContextInitialize;
</#macro>

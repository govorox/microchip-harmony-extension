/*******************************************************************************
  MPLAB Harmony Generated Driver Implementation File

  File Name:
    drv_gfx_ili9341_intf.c

  Summary:
    Implements DBIB parallel interface for the ILI9341

  Description:
    Implements DBIB parallel interface for the ILI9341. This driver uses the PMP
    peripheral port to drive the parallel interface.

    Created with MPLAB Harmony Version ${CONFIG_MPLAB_HARMONY_VERSION_STRING}
 *******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2017 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

#include "system_config.h"
#include "system_definitions.h"

#include "gfx/hal/inc/gfx_common.h"
#include "gfx/hal/inc/gfx_context.h"

#include "drv_gfx_ili9341_cmd_defs.h"
#include "drv_gfx_ili9341_common.h"

// This is the base address of the PMP0 peripheral on V71.
// Update to appropriate base address for other MCUs.
//#define ILI9341_DBIB_BASE_ADDR  0x60000000
//#define ILI9341_DBIB_CMD_ADDR  ILI9341_DBIB_BASE_ADDR

// Use PMP index ${CONFIG_DRV_GFX_ILI9341_PMP_PORT}

#define GFX_PMP_Read    DRV_PMP${CONFIG_DRV_GFX_ILI9341_PMP_PORT}_Read
#define GFX_PMP_Write   DRV_PMP${CONFIG_DRV_GFX_ILI9341_PMP_PORT}_Write

<#if CONFIG_DRV_GFX_CONTROLLER_TYPE == "ILI9341 (16-bit Parallel/PMP)">
// Data width for 16-bit PMP
typedef uint16_t DBUS_WIDTH_T;
</#if>
<#if CONFIG_DRV_GFX_CONTROLLER_TYPE == "ILI9341 (8-bit Parallel/PMP)">
// Data width for 8-bit PMP
typedef uint8_t DBUS_WIDTH_T;
</#if>

typedef struct
{
    void * ptr;
} ILI9341_PMP_PRIV;


/**
  Function:
    static GFX_Result ILI9341_Intf_Read(struct ILI9341_DRV *drv,
                                       uint8_t cmd,
                                       uint8_t *data,
                                       int bytes)

  Summary:
    Sends read command and returns response from the ILI9341 device.

  Description:
    This function will do an PMP write operation to send the read command
    to the ILI9341, and then do a PMP read operation to read the response.

  Parameters:
    drv         - ILI9341 driver handle
    cmd         - Read command
    data        - Buffer to store read data
    bytes       - Number of bytes to read

Returns:
    * GFX_SUCCESS       - Operation successful
    * GFX_FAILURE       - Operation failed


 */
static GFX_Result ILI9341_Intf_Read(struct ILI9341_DRV *drv,
                                    uint8_t cmd,
                                    uint8_t *data,
                                    int bytes)
{
    unsigned int i;

    if ((!drv) || (!data) || (bytes <= 0))
        return GFX_FAILURE;

    ILI9341_PMP_DCX_Command();
    ILI9341_NCSAssert();

    // Write the command
    GFX_PMP_Write(cmd);

    ILI9341_PMP_DCX_Data();

    // Read data 1 byte a time
    for (i = 0; i < bytes; i++)
    {
        *(data + i) = GFX_PMP_Read();
    }

    ILI9341_PMP_DCX_Command();
    ILI9341_NCSDeassert();

    return GFX_SUCCESS;;
}

/**
  Function:
    GFX_Result ILI9341_Intf_WriteCmd(struct ILI9341_DRV *drv,
                                    uint8_t cmd,
                                    uint8_t *parms,
                                    int num_parms)

  Summary:
    Sends write command and parameters to the ILI9341 device.

  Description:

    This function will do a PMP write operation to send the write command
    and its parameters to the ILI9341.


  Parameters:
    drv         - ILI9341 driver handle
    cmd         - Read command
    parms       - Pointer to array of 8-bit parameters
    bytes       - Number of command parameters

Returns:
    * GFX_SUCCESS       - Operation successful
    * GFX_FAILURE       - Operation failed


 */
GFX_Result ILI9341_Intf_WriteCmd(struct ILI9341_DRV *drv,
                                uint8_t cmd,
                                uint8_t *parms,
                                int num_parms)
{
    unsigned int i;

    if (!drv)
        return GFX_FAILURE;

    ILI9341_PMP_DCX_Command();
    ILI9341_NCSAssert();

    // Write the command
    GFX_PMP_Write(cmd);

    ILI9341_PMP_DCX_Data();

    // Write data one byte at a time
    for (i = 0; i < num_parms; i++)
    {
        GFX_PMP_Write(*(parms));
        parms++;
    }

    ILI9341_PMP_DCX_Command();
    ILI9341_NCSDeassert();

    return GFX_SUCCESS;
}

/**
  Function:
    GFX_Result ILI9341_Intf_WritePixels(struct ILI9341_DRV *drv,
                                              uint32_t start_x,
                                              uint32_t start_y,
                                              uint8_t *data,
                                              unsigned int num_pixels)

  Summary:
    Writes pixel data to ILI9341 GRAM from specified position.

  Description:
    This function will first write the start column, page information, then
    write the pixel data to the ILI9341 GRAM.

  Parameters:
    drv             - ILI9341 driver handle
    start_x         - Start column position
    start_y         - Start page position
    data            - Array of 8-bit pixel data (8-bit/pixel RGB)
    num_pixels      - Number of pixels

  Returns:
    * GFX_SUCCESS       - Operation successful
    * GFX_FAILURE       - Operation failed

 */
GFX_Result ILI9341_Intf_WritePixels(struct ILI9341_DRV *drv,
                                   uint32_t start_x,
                                   uint32_t start_y,
                                   uint8_t *data,
                                   unsigned int num_pixels)
{
    GFX_Result returnValue = GFX_FAILURE;
    uint8_t buf[4];

    if (!drv)
        return GFX_FAILURE;

    //Set column
    buf[0] = (start_x >> 8);
    buf[1] = (start_x & 0xff);
    buf[2] = (((drv->gfx->display_info->rect.width - 1) & 0xff00) >> 8);
    buf[3] = ((drv->gfx->display_info->rect.width - 1) & 0xff);
    returnValue = ILI9341_Intf_WriteCmd(drv,
                                       ILI9341_CMD_COLUMN_ADDRESS_SET,
                                       buf,
                                       4);
    if (GFX_SUCCESS != returnValue)
        return GFX_FAILURE;

    //Set page
    buf[0] = (start_y >> 8);
    buf[1] = (start_y & 0xff);
    buf[2] = (((drv->gfx->display_info->rect.height - 1) & 0xff00) >> 8);
    buf[3] = ((drv->gfx->display_info->rect.height - 1) & 0xff);
    returnValue = ILI9341_Intf_WriteCmd(drv,
                                       ILI9341_CMD_PAGE_ADDRESS_SET,
                                       buf,
                                       4);
    if (GFX_SUCCESS != returnValue)
        return GFX_FAILURE;

<#if CONFIG_DRV_GFX_CONTROLLER_TYPE == "ILI9341 (16-bit Parallel/PMP)">

</#if>
<#if CONFIG_DRV_GFX_CONTROLLER_TYPE == "ILI9341 (8-bit Parallel/PMP)">
    returnValue = ILI9341_Intf_WriteCmd(drv,
                                       ILI9341_CMD_MEMORY_WRITE,
                                       data,
                                       num_pixels * 2);
</#if>

    return returnValue;
}

/**
  Function:
    GFX_Result ILI9341_Intf_ReadPixels(struct ILI9341_DRV *drv,
                                      uint32_t x,
                                      uint32_t y,
                                      uint16_t *value,
                                      unsigned int num_pixels)

  Summary:
    Read pixel data from specified position in ILI9341 GRAM.

  Description:
    This function will first write the start column, page information, then
    read the pixel data from the ILI9341 GRAM.

  Parameters:
    drv             - ILI9341 driver handle
    x               - Column position
    y               - Page position
    value           - Value to store the read pixel color (8-bit/pixel RGB)
    num_pixels      - Number of pixels to read

  Returns:
    * GFX_SUCCESS       - Operation successful
    * GFX_FAILURE       - Operation failed

 */
GFX_Result ILI9341_Intf_ReadPixels(struct ILI9341_DRV *drv,
                                  uint32_t x,
                                  uint32_t y,
                                  uint8_t *value,
                                  unsigned int num_pixels)
{
    GFX_Result returnValue = GFX_FAILURE;
    uint8_t buf[4];
    unsigned int i;
    volatile DBUS_WIDTH_T pixel[3] = {0};

    if (!drv)
        return GFX_FAILURE;

    //Set column
    buf[0] = ((x & 0xff00) >> 8);
    buf[1] = (x & 0xff);
    buf[2] = (((drv->gfx->display_info->rect.width - 1) & 0xff00) >> 8);
    buf[3] = ((drv->gfx->display_info->rect.width - 1) & 0xff);
    returnValue = ILI9341_Intf_WriteCmd(drv,
                                       ILI9341_CMD_COLUMN_ADDRESS_SET,
                                       buf,
                                       4);
    if (GFX_SUCCESS != returnValue)
        return GFX_FAILURE;

    //Set page
    buf[0] = ((y & 0xff00) >> 8);
    buf[1] = (y & 0xff);
    buf[2] = (((drv->gfx->display_info->rect.height - 1) & 0xff00) >> 8);
    buf[3] = ((drv->gfx->display_info->rect.height - 1) & 0xff);
    returnValue = ILI9341_Intf_WriteCmd(drv,
                                       ILI9341_CMD_PAGE_ADDRESS_SET,
                                       buf,
                                       4);
    if (GFX_SUCCESS != returnValue)
        return GFX_FAILURE;

    ILI9341_NCSAssert();

    ILI9341_PMP_DCX_Command();
    GFX_PMP_Write(ILI9341_CMD_MEMORY_READ);

    ILI9341_PMP_DCX_Data();

    // Read the dummy byte
    pixel[0] = GFX_PMP_Read();

    // Read the pixel data
<#if CONFIG_DRV_GFX_CONTROLLER_TYPE == "ILI9341 (16-bit Parallel/PMP)">

</#if>
<#if CONFIG_DRV_GFX_CONTROLLER_TYPE == "ILI9341 (8-bit Parallel/PMP)">
    for (i = 0; i < num_pixels; i++)
    {
        // In 8-bit mode, each 8-bit read gets one color in RGB565 color mode.
        // Read 3 bytes to get R, G, and B.
        pixel[0] = GFX_PMP_Read(); //R
        pixel[1] = GFX_PMP_Read(); //G
        pixel[2] = GFX_PMP_Read(); //B

        value[i * drv->bytesPerPixelBuffer] = (pixel[0] | (pixel[1] >> 5));
        value[(i * drv->bytesPerPixelBuffer) + 1] =
                                    ((pixel[1] & 0x1c) << 3) | (pixel[2] >> 3);
    }
</#if>

    ILI9341_PMP_DCX_Command();
    ILI9341_NCSDeassert();

    return GFX_SUCCESS;
}

/**
  Function:
    GFX_Result ILI9341_Intf_ReadCmd(struct ILI9341_DRV *drv,
                                          uint8_t cmd,
                                          uint8_t *data,
                                          int bytes);

  Summary:
    Sends read command and reads response from ILI9341.

  Description:
    This function will fist write the the read command and then read back the
    response from the ILI9341 GRAM.

  Parameters:
    drv             - ILI9341 driver handle
    cmd             - Read command
    data            - Buffer to store the read data to
    bytes           - Number of bytes to read

  Returns:
    * GFX_SUCCESS       Operation successful
    * GFX_FAILURE       Operation failed

  Remarks:
    This function only supports 8-, 24- or 32-bit reads.

 */
GFX_Result ILI9341_Intf_ReadCmd(struct ILI9341_DRV *drv,
                               uint8_t cmd,
                               uint8_t *data,
                               int bytes)
{
    GFX_Result returnValue = GFX_FAILURE;
    uint8_t buff[5];

    //API supports only 8-, 24-, or 32-bit reads
    if ((!drv) || (!data) ||
        ((bytes != 1) && (bytes != 3) && (bytes != 4)))
        return GFX_FAILURE;

    returnValue = ILI9341_Intf_Read(drv, cmd, buff, bytes + 1);

    return returnValue;
}

// *****************************************************************************

/**
  Function:
    GFX_Result ILI9341_Intf_Open(ILI9341_DRV *drv, unsigned int index)

  Summary:
    Opens the specified port to the ILI9341 device.

  Description:
    In SPI mode, this function will open the SPI port, allocate the port-specific
    data structures and set the port operation handler functions. When done
    using the port, ILI9341_Intf_Close must be called to free up the data
    structures and close the port.

  Parameters:
    drv         - ILI9341 driver handle
    index       - Port index

  Returns:
    * GFX_SUCCESS       - Operation successful
    * GFX_FAILURE       - Operation failed

 */
GFX_Result ILI9341_Intf_Open(ILI9341_DRV *drv, unsigned int index)
{
    ILI9341_PMP_PRIV *dbiPriv = NULL;

    if (!drv)
        return GFX_FAILURE;

    dbiPriv = (ILI9341_PMP_PRIV *)
                drv->gfx->memory.calloc(1, sizeof (ILI9341_PMP_PRIV));

    drv->port_priv = (void *) dbiPriv;

    ILI9341_NCSDeassert();

    return GFX_SUCCESS;
}

/**
  Function:
    void ILI9341_Intf_Close(ILI9341_DRV *drv)

  Summary:
    Closes the HW interface to the ILI9341 device.

  Description:
    This function will close the specified interface, free the port-specific
    data structures and unset the port operation handler functions.

  Parameters:
    drv         - ILI9341 driver handle

  Returns:
    None.

 */
void ILI9341_Intf_Close(ILI9341_DRV *drv)
{
    ILI9341_PMP_PRIV *dbiPriv = NULL;

    if (!drv)
        return;

    dbiPriv = (ILI9341_PMP_PRIV *) drv->port_priv;

    drv->gfx->memory.free(dbiPriv);

    drv->port_priv = NULL;

    ILI9341_NCSDeassert();
}
/* *****************************************************************************
 End of File
 */
